<div class="be-content">
            <div class="page-head">
              <h2 class="page-head-title"><?= $title ?></h2>
              <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb page-head-nav">
                  <li class="breadcrumb-item active"><?= $title ?></li>
                </ol>
              </nav>
            </div>
              <div class="main-content container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
                        <div class="card card-border-color card-border-color-primary">
                            <div class="card-body">
                                <form action="<?php echo $action; ?>" method="post">
	                                    <div class="form-group">
                                            <label for="varchar">Nama <?php echo form_error('nama') ?></label>
                                            <input type="text" class="form-control form-control-xs" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
                                        </div>
	                                   <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	                                   <button type="submit" class="btn btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> <?php echo $button ?></button> 
	                                   <a class="btn btn-sm btn-warning" href="<?php echo site_url('test') ?>" ><i class="mdi mdi-close"></i> Cancel</a>
	                            </form>
                            </div>                                                      
                        </div><!-- end card-->                  
                    </div>
                </div>
            </div>
        </div>