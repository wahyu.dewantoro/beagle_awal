<link rel="stylesheet" type="text/css" href="<?= base_url() ?>lipstick/assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>lipstick/assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>lipstick/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>lipstick/assets/lib/select2/css/select2.min.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>lipstick/assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
<link rel="stylesheet" href="<?= base_url() ?>lipstick/assets/css/app.css" type="text/css"/>