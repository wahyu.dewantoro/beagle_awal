<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?= base_url() ?>lipstick/assets/img/logo-fav.png">
    <title>Beagle</title>
    <?php 
    if(isset($page)){
      if($page=='table'){
      $this->load->view('partial/head_table'); 
     }else if ($page=='form'){
      $this->load->view('partial/head_form'); 
     } else { 
        $this->load->view('partial/head_default'); 
     }

   }else{
        $this->load->view('partial/head_default'); 
   }
      ?>
   

  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <?php $this->load->view('partial/navbar_default');?>
      <?php $this->load->view('partial/sidebar'); ?>
      <?= $contents ?>
    
    </div>
    <?php 
      if(isset($page)){
        if($page=='table'){
        $this->load->view('partial/js_table'); 
      }else if($page=='form'){
        $this->load->view('partial/js_form'); 
      }else{
        $this->load->view('partial/js_default');   
      }

    }else{
        $this->load->view('partial/js_default');   
    }
      
    ?>
    
  </body>
</html>