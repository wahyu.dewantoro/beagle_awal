<script src="<?= base_url() ?>lipstick/assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>lipstick/assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>lipstick/assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>lipstick/assets/js/app.js" type="text/javascript"></script>
<script src="<?= base_url() ?>lipstick/assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>lipstick/assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>lipstick/assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>lipstick/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>lipstick/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>lipstick/assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>lipstick/assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    //-initialize the javascript
    App.init();
    App.formElements();


  });

  <?php if($this->uri->segment(1)=='pengguna'){?>

  $(document).ready(function () {

                $('#password, #confirm').on('keyup', function () {
                  if($('#password').val()!='' && $('#confirm').val()!=''){
                      if ($('#password').val() == $('#confirm').val()) {
                        $('#message').html('Matching').css('color', 'green');
                        $("#btn").prop('disabled', false);
                      } else {
                        $('#message').html('Not Matching').css('color', 'red');
                         $("#btn").prop('disabled', true);
                      }

                    }else{
                      $("#btn").prop('disabled', true);
                    }

                });


            $('#submitbtn').click(function() {

            	if($('#confirm').val()!=$('#password').val()){
            		alert("password Not Matching");
                return false;
            	}

              
             	checked = $("input[type=checkbox]:checked").length;
              if(checked==0) {
                alert("You must check at least one checkbox.");
                return false;
              }

            });
        });
<?php } ?>
</script>