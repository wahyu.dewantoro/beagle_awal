<div class="be-content">
        <div class="page-head">
          <h2 class="page-head-title"><?= $title ?></h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="#">Sistem</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url().'role'?>">Role</a></li>
              <li class="breadcrumb-item active"><?= $title ?></li>
            </ol>
          </nav>
        </div>
          <div class="main-content container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
                    <div class="card card-table">
                        <div class="card-body">
            			
                        	<form method="post" action="<?php echo base_url().'role/prosessettingrole'?>">
                                <button class="btn float-right btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> Submit</button>
                                <input type="hidden" name="kode_group"  value="<?php echo $id_inc;?>">


                                <table  class="table table-bordered table-hover">
                                  <thead>
                                    <tr>
                                      <th colspan="4">Akses</th>
                                      <th rowspan="2">menu</th>
                                      <th rowspan="2">Parent</th>
                                    </tr>
                                    <tr>
                                      <td width="3%">Read</td>
                                      <td width="3%">Create</td>
                                      <td width="3%">Update</td>
                                      <td width="3%">Delete</td>
                                    </tr>
                                  </thead>
                                  <tbody>
                                      <?php 
                                      foreach($role as $row){?>
                                    <tr>
                                      <td align="center">
                                        <label class="be-checkbox custom-control custom-checkbox">
                                       <input <?php if($row['STATUS']==1){ echo "checked";}?> type="checkbox"  name="role[]" value="<?php echo $row['kode_role']; ?>">
                                     </label>
                                     </td>
                                      <td align="center">
                                        <label class="be-checkbox custom-control custom-checkbox">
                                       <input <?php if($row['create']==1){ echo "checked";}?> type="checkbox"  name="create[<?php echo $row['kode_role']; ?>]" value="1">
                                     </label>
                                     </td>
                                      <td align="center">
                                        <label class="be-checkbox custom-control custom-checkbox">
                                       <input <?php if($row['update']==1){ echo "checked";}?> type="checkbox"  name="update[<?php echo $row['kode_role']; ?>]" value="1">
                                     </label>
                                     </td>
                                      <td align="center">
                                        <label class="be-checkbox custom-control custom-checkbox">
                                       <input <?php if($row['delete']==1){ echo "checked";}?> type="checkbox"  name="delete[<?php echo $row['kode_role']; ?>]" value="1">
                                     </label>
                                     </td>
                                      <td><?php echo ucwords($row['nama_menu']);?></td>
                                      <td><?php echo ucwords($row['parent']);?></td>
                                    </tr>
                                  <?php }?>
                                  </tbody>
                                </table>
                            </form>

                         </div>                                                      
                    </div><!-- end card-->                  
                </div>
            </div>
  </div>
</div>