<div class="be-content">
        <div class="page-head">
          <h2 class="page-head-title"><?= $title ?></h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="#">Sistem</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url().'role'?>">Role</a></li>
              <li class="breadcrumb-item active"><?= $title ?></li>
            </ol>
          </nav>
        </div>
          <div class="main-content container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-12 col-xl-6">
                    <div class="card card-border-color card-border-color-primary">
                        <div class="card-body">
                            <form action="<?php echo $action; ?>" method="post">
            	    <div class="form-group">
                        <label for="varchar">Nama Role <?php echo form_error('nama_role') ?></label>
                        <input type="text" class="form-control form-control-xs" name="nama_role" id="nama_role" placeholder="Nama Role" value="<?php echo $nama_role; ?>" />
                    </div>
            	    <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" /> 
            	    <button type="submit" class="btn btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> <?php echo $button ?></button> 
            	    <a class="btn btn-sm btn-warning" href="<?php echo site_url('role') ?>" ><i class="mdi mdi-close"></i> Cancel</a>
            	</form>
                        </div>                                                      
                    </div><!-- end card-->                  
                </div>
            </div>
    </div>
</div>