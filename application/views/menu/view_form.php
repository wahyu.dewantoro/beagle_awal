<div class="be-content">
            <div class="page-head">
              <h2 class="page-head-title"><?= $title ?></h2>
              <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb page-head-nav">
                  <li class="breadcrumb-item">Sistem</li>
                  <li class="breadcrumb-item"><?= anchor('menu','Menu')?></li>
                  <li class="breadcrumb-item active"><?= $title ?></li>
                </ol>
              </nav>
            </div>
              <div class="main-content container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">                       
                        <div class="card card-border-color card-border-color-primary">
                            <div class="card-body">
                                <form action="<?php echo $action; ?>" method="post">
	                                    <div class="form-group">
                                            <label for="varchar">Nama Menu <?php echo form_error('nama_menu') ?></label>
                                            <input type="text" class="form-control form-control-xs" name="nama_menu" id="nama_menu" placeholder="Nama Menu" value="<?php echo $nama_menu; ?>" />
                                        </div>
	                                    <div class="form-group">
                                            <label for="varchar">Link Menu <?php echo form_error('link_menu') ?></label>
                                            <input type="text" class="form-control form-control-xs" name="link_menu" id="link_menu" placeholder="Link Menu" value="<?php echo $link_menu; ?>" />
                                        </div>
	                                    <div class="form-group">
                                            <label for="int">Parent <?php echo form_error('parent') ?></label>
                                            <select class="form-control form-control-xs" name="parent" id="parent" placeholder="Parent">
                                                <option value="">Pilih</option>
                                                <?php foreach($parent_data as $pd){?>
                                                <option <?php if($parent==$pd->id_inc){echo "selected";}?> value="<?= $pd->id_inc ?>"><?= $pd->nama_menu?></option>
                                                <?php } ?>
                                            </select>

                                            <!-- <input type="text"  value="<?php echo $parent; ?>" /> -->
                                        </div>
	                                    <div class="form-group">
                                            <label for="int">Sort <?php echo form_error('sort') ?></label>
                                            <input type="text" class="form-control form-control-xs" name="sort" id="sort" placeholder="Sort" value="<?php echo $sort; ?>" />
                                        </div>
	                                    <div class="form-group">
                                            <label for="varchar">Icon <?php echo form_error('icon') ?></label>
                                            <input type="text" class="form-control form-control-xs" name="icon" id="icon" placeholder="Icon" value="<?php echo $icon; ?>" />
                                        </div>
	                                   <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" /> 
	                                   <button type="submit" class="btn btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> <?php echo $button ?></button> 
	                                   <a class="btn btn-sm btn-warning" href="<?php echo site_url('menu') ?>" ><i class="mdi mdi-close"></i> Cancel</a>
	                            </form>
                            </div>                                                      
                        </div><!-- end card-->                  
                    </div>
                </div>
            </div>
        </div>