<div class="be-content">
            <div class="page-head">
              <h2 class="page-head-title"><?= $title ?></h2>
              <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb page-head-nav">
                  <li class="breadcrumb-item ">Sistem</li>
                  <li class="breadcrumb-item "><?= anchor('pengguna','Pengguna')?></li>
                  <li class="breadcrumb-item active"><?= $title ?></li>
                </ol>
              </nav>
            </div>
              <div class="main-content container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="card card-border-color card-border-color-primary">
                            <div class="card-body">
                                <form action="<?php echo $action; ?>" method="post">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="varchar">Nama <?php echo form_error('nama') ?></label>
                                                <input type="text" class="form-control form-control-xs" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
                                            </div>
                                            <div class="form-group">
                                                <label for="varchar">Username <?php echo form_error('username') ?></label>
                                                <input type="text" class="form-control form-control-xs" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
                                            </div>
                                            <?php if($id_inc==''){?>

                                            <div class="form-group">
                                                <label for="varchar">Password <?php echo form_error('password') ?></label>
                                                <input type="password" class="form-control form-control-xs" name="password" id="password" placeholder="Password"  />
                                            </div>
                                            <div class="form-group">
                                                <label for="varchar">Konfirmasi </label>
                                                <input type="password" class="form-control form-control-xs" name="confirm" id="confirm" placeholder="Password konfirmasi"  />
                                                <div id="message"></div>
                                            </div>
                                            <?php } ?> 
                                        </div>
                                        <div class="col-md-6">
                                            <label for="varchar">Assign To  </label>
                                            <div class="checkbox-group required">
                                            <?php foreach($role as $rl){?>
                                                <label class="be-checkbox custom-control custom-checkbox">
                                                  <input <?php if(in_array($rl->id_inc,$vrole)){echo "checked";}?> class="custom-control-input" id="<?php echo $rl->nama_role ?>" name="assign[]" value="<?= $rl->id_inc?>" type="checkbox"><span class="custom-control-label"><?php echo $rl->nama_role ?></span>
                                                </label>
                                            <?php } ?>
                                            </div>
                                            
                                        </div>
                                    </div>



	                                    
	                                   <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" /> 
	                                   <button type="submit"  id="submitbtn"   class="btn btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> <?php echo $button ?></button> 

	                                   <a class="btn btn-sm btn-warning" href="<?php echo site_url('pengguna') ?>" ><i class="mdi mdi-close"></i> Cancel</a>
	                            </form>
                            </div>                                                      
                        </div><!-- end card-->                  
                    </div>
                </div>
            </div>
        </div>
