<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('acak'))
{
	function acak($str) {
		    $kunci = '==$#$$@#$jsdjGHFVGHSDF&*672367235HHGDVHVDyhagsvfhg^%$#@#@#&^*(*^&^&}{P{P&*^&*()(';
		    $hasil='';
		    for ($i = 0; $i < strlen($str); $i++) {
		        $karakter = substr($str, $i, 1);
		        $kuncikarakter = substr($kunci, ($i % strlen($kunci))-1, 1);
		        $karakter = chr(ord($karakter)+ord($kuncikarakter));
		        $hasil .= $karakter;
		        
		    }
		    return urlencode(base64_encode($hasil));
		}

}

if( ! function_exists('rapikan'))
{
	function rapikan($str) {
		    $str = base64_decode(urldecode($str));
		    $hasil = '';
		    $kunci = '==$#$$@#$jsdjGHFVGHSDF&*672367235HHGDVHVDyhagsvfhg^%$#@#@#&^*(*^&^&}{P{P&*^&*()(';
		    for ($i = 0; $i < strlen($str); $i++) {
		        $karakter = substr($str, $i, 1);
		        $kuncikarakter = substr($kunci, ($i % strlen($kunci))-1, 1);
		        $karakter = chr(ord($karakter)-ord($kuncikarakter));
		        $hasil .= $karakter;
		        
		    }
		    return $hasil;
		}
}


if (!function_exists('get_flashdata'))
{
    function get_flashdata($key)
    {
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->flashdata($key);
    }
}
if (!function_exists('set_flashdata'))
{
    function set_flashdata($type, $msg)
    {
        $CI = get_instance();
        $CI->load->library('session');

        if($type=='success'){
            $icon='mdi mdi-check';
        }else{
            $icon='mdi mdi-alert-triangle';
        }

        $CI->session->set_flashdata('message', '<div class="alert alert-contrast alert-'.$type.' alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="'.$icon.'"></span></div>
                    <div class="message">'.$msg.'</div>
                  </div>');   

        // return $CI->session->set_flashdata($key, $value);
    }
}
if (!function_exists('keep_flashdata'))
{
    function keep_flashdata($key)
    {
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->keep_flashdata($key);
    }
}
if (!function_exists('get_userdata'))
{
    function get_userdata($key)
    {
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->userdata($key);
    }
}
if (!function_exists('all_userdata'))
{
    function all_userdata()
    {
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->all_userdata();
    }
}
if (!function_exists('set_userdata'))
{
    function set_userdata($key, $value)
    {
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->set_userdata($key, $value);
    }
}
if (!function_exists('unset_userdata'))
{
    function unset_userdata($data)
    {
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->unset_userdata($data);
    }
}
if (!function_exists('session_destroy'))
{
    function session_destroy()
    {
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->sess_destroy();
    }
}

if (!function_exists('showMenu'))
{
    function showMenu($kode_group)
    {
        //  $this->uri->segment(1) == 'products' ? 'active': '' 

         $CI = get_instance();
         $CI->load->database(); 
        $qmenu0  =$CI->db->query("SELECT a.*
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'  
                                          AND parent='0' 
                                          ORDER BY sort ASC")->result_array();

        $varmenu='';
              foreach ($qmenu0 as $row0) {
                $parent  =$row0['id_inc'];
                $qmenu1  =$CI->db->query("SELECT a.*
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'  
                                          AND parent='$parent' 
                                          ORDER BY sort ASC");
                $cekmenu =$qmenu1->num_rows();
                $dmenu1  =$qmenu1->result_array();
                  if($cekmenu>0){
                    $varmenu.= "<li class='parent'>
                      <a href='#' > <i class='".$row0['icon']."'></i>  <span>".ucwords($row0['nama_menu'])."</span> <span class='menu-arrow'></span></a>";
                        $varmenu.= "<ul class='sub-menu'>";
                          foreach($dmenu1 as $row1){
                            $qwa=explode('/',$row1['link_menu']);
                            $dda=strtolower($CI->uri->segment(1)) == strtolower($qwa[0]) ? 'active': '' ;    
                              $varmenu.= "<li class='".$dda."'>".anchor(strtolower($row1['link_menu']),"<i class='mdi mdi-chevron-right'></i> <span>".ucwords($row1['nama_menu']."</span>"),'class="nav-link "')."</li>";
                          }
                      $varmenu.= "</ul>
                      </li>";
                      }else{
                        $qaw=explode('/',$row0['link_menu']);
                        
                            $dd=strtolower($CI->uri->segment(1)) == strtolower($qaw[0]) ? 'active': '' ;    
                        

                        
                       $varmenu.= "<li class='".$dd."'>".anchor(strtolower($row0['link_menu']),"<i class='".$row0['icon']."'></i> <span>".ucwords($row0['nama_menu']).'</span>','class="nav-link "')."</li>";
                    }
                  }
           echo $varmenu;
    }
}



if (!function_exists('errorbos'))
{
    function errorbos()
    {
            $CI = &get_instance();
            $CI->load->view('errors/403','',true);
            die();
    }
}

if (!function_exists('cek'))
{
    function cek($pengguna,$url,$var)
    {
        $CI = get_instance();
        $CI->load->database(); 

        if(empty($var)){
            errorbos();
        }
        switch ($var) {
            case 'read':
                # code...
                $vv='status';
                break;      
            default:
                # code...
                $vv=$var;
                break;
        }

        $query=$CI->db->query("SELECT a.*
                                FROM ms_privilege a
                                JOIN ms_menu b ON a.ms_menu_id=b.id_inc
                                JOIN ms_assign_role d on d.ms_role_id=a.ms_role_id
                                JOIN ms_pengguna c ON c.id_inc=d.ms_pengguna_id
                                WHERE c.id_inc=$pengguna AND REPLACE(link_menu,'/','.')='$url' AND a.".$vv." ='1'");
        $hitung=$query->num_rows();
        if($hitung > 0){
          $res=$query->row();

                return array(
                    'read'   =>$res->status,
                    'create' =>$res->create,
                    'update' =>$res->update,
                    'delete' =>$res->delete
                );

        }else{
            errorbos();
        }
    }
}


/*if (!function_exists('set_value')) {
 
        function set_value($field = '', $default = '') {
                $OBJ = & _get_validation_object();
 
                if ($OBJ === TRUE && isset($OBJ->_field_data[$field])) {
                        return form_prep($OBJ->set_value($field, $default));
                } else {
                        if (!isset($_POST[$field])) {
                                return $default;
                        }
 
                        return form_prep($_POST[$field]);
                }
        }
 
}*/
 
/*if (!function_exists('set_checkbox')) {
 
        function set_checkbox($field = '', $value = '', $default = '') {
                $OBJ = & _get_validation_object();
 
                if ($OBJ === TRUE && isset($OBJ->_field_data[$field])) {
                        return form_prep($OBJ->set_checkbox($field, $value, $default));
                } else {
                        if (!isset($_POST[$field])) {
                                return '';
                        }
                        else{
                                if($_POST[$field] == $value)
                                {
                                        return 'checked=\'checked\'';
                                }
                        }
                }
        }
 
}
 */