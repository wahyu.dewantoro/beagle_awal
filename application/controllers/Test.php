<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Test extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('id_pengguna');
        $this->load->model('Test_model');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Test';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url']  = base_url() . 'test?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'test?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'test';
            $config['first_url'] = base_url() . 'test';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Test_model->total_rows($q);
        $test                      = $this->Test_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'test_data' => $test,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'test',
            'akses'               => $akses,
            'page'                =>'table'
        );
        $this->template->load('partial/konten_layout','test/view_index',$data);
    }

    public function read($ide) 
    {
        $this->cekAkses('read');
        $id  =rapikan($ide);
        $row = $this->Test_model->get_by_id($id);
        if ($row) {
            $data = array(
                'title'=>'Detail test',
		'id' => $row->id,
		'nama' => $row->nama,
	    );
            $this->template->load('partial/konten_layout','test/view_read',$data);
        } else {
            set_flashdata('warning','Record Not Found');
            redirect(site_url('test'));
        }
    }

    public function create() 
    {
        $this->cekAkses('create');
        $data = array(
            'page'=>'form',
            'button' => 'Simpan',
            'title'  => 'Tambah test',
            'action' => site_url('test/create_action'),
	    'id' => set_value('id'),
	    'nama' => set_value('nama'),
	);
        $this->template->load('partial/konten_layout','test/view_form',$data);
    }
    
    public function create_action() 
    {
        $this->cekAkses('create');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
	    );

            $res=$this->Test_model->insert($data);

            if($res){
                set_flashdata('success','Data berhasil di simpan.');
            }else{
                set_flashdata('warning','Data gagal di simpan.');
            }
            

            redirect(site_url('test'));
        }
    }
    
    public function update($ide) 
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Test_model->get_by_id($id);
        
        if ($row) {
            $data = array(
                'page'=>'form',
                'title'  => 'Edit test',
                'button' => 'Update',
                'action' => site_url('test/update_action'),
		'id' => set_value('id', $row->id),
		'nama' => set_value('nama', $row->nama),
	    );
            // $this->load->view('test/view_form', $data);
            $this->template->load('partial/konten_layout','test/view_form',$data);
        } else {
                set_flashdata('warning','Record Not Found');
                redirect(site_url('test'));
        }
    }
    
    public function update_action() 
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
	    );

            $res=$this->Test_model->update($this->input->post('id', TRUE), $data);
            if($res){
                set_flashdata('success','Data berhasil di update.');
            }else{
                set_flashdata('warning','Data gagal di update.');
            }
            redirect(site_url('test'));
        }
    }
    
    public function delete($ide) 
    {
        $this->cekAkses('delete');
        $id  =rapikan($ide);
        $row = $this->Test_model->get_by_id($id);

        if ($row) {
            $res=$this->Test_model->delete($id);
            if($res){
                set_flashdata('success','Data berhasil di hapus.');
            }else{
                set_flashdata('warning','Data gagal di hapus.');
            }
            redirect(site_url('test'));
        } else {
            set_flashdata('warning','Record Not Found');
            redirect(site_url('test'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Test.php */
/* Location: ./application/controllers/Test.php */