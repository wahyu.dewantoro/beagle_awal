<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('id_pengguna');
        $this->load->model('Model_pengguna');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Pengguna';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url']  = base_url() . 'pengguna?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pengguna?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'pengguna';
            $config['first_url'] = base_url() . 'pengguna';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Model_pengguna->total_rows($q);
        $pengguna                      = $this->Model_pengguna->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pengguna_data' => $pengguna,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Pengguna',
            'akses'               => $akses,
            'page'                =>'table'
        );
        // echo $this->uri->segment(1);
        $this->template->load('partial/konten_layout','pengguna/view_index',$data);
    }

    public function read($ide) 
    {
        $this->cekAkses('read');
        $id  =rapikan($ide);
        $row = $this->Model_pengguna->get_by_id($id);
        if ($row) {
            $data = array(
                'title'=>'Profil',
                'id_inc' => $row->id_inc,
                'nama' => $row->nama,
                'username' => $row->username,
                'password' => $row->password,
	       );
            $this->template->load('partial/konten_layout','pengguna/view_read',$data);
        } else {
            set_flashdata('warning','Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function create() 
    {
        $this->cekAkses('create');
        $data = array(
            'page'     =>'form',
            'button'   => 'Simpan',
            'title'    => 'Tambah Pengguna',
            'action'   => site_url('pengguna/create_action'),
            'id_inc'   => set_value('id_inc'),
            'nama'     => set_value('nama'),
            'username' => set_value('username'),
            'password' => set_value('password'),
            'role'     =>$this->Model_pengguna->get_role(),
            'vrole'    =>array()
            
	       );
        $this->template->load('partial/konten_layout','pengguna/view_form',$data);
    }
    
    public function create_action() 
    {
        $this->cekAkses('create');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            
            // echo $this->cek_role();
            if($this->cek_role()==0){
                set_flashdata('warning','Data gagal di simpan. karena belum memilih role');
                redirect(site_url('pengguna'));
            }

            if($this->cek_pass()==0){
                set_flashdata('warning','Data gagal di simpan. karena password tidak sama');
                redirect(site_url('pengguna'));
            }


            
            $this->db->trans_start();
            $data = array(
                'nama'            => $this->input->post('nama',TRUE),
                'username'        => $this->input->post('username',TRUE),
                'password'        => sha1($this->input->post('password',TRUE)),
                'pengguna_insert' =>$this->id_pengguna
        	    );
            $this->Model_pengguna->insert($data);

            $this->db->where('pengguna_insert',$this->id_pengguna);
            $this->db->select("max(id_inc) id_pengguna",false);
            $ms_pengguna_id=$this->db->get('ms_pengguna')->row()->id_pengguna;

            for($i=0;$i<count($this->input->post('assign')); $i++){
                $role=array(
                    'ms_pengguna_id'=>$ms_pengguna_id,
                    'ms_role_id'=>$this->input->post('assign')[$i]
                );
                $this->db->insert('ms_assign_role',$role);
            }

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE){
                set_flashdata('warning','Data gagal di simpan.');
            
            }else{
                set_flashdata('success','Data berhasil di simpan.');
            }
            redirect(site_url('pengguna'));
        }
    }
    
    public function update($ide) 
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Model_pengguna->get_by_id($id);
        
        if ($row) {

            $lrole=$this->db->query("SELECT * FROM ms_assign_role WHERE ms_pengguna_id=".$row->id_inc)->result();
            $vrole=array();
            foreach ($lrole as $lrole) {
                array_push($vrole,$lrole->ms_role_id);
            }


            $data = array(
                'page'     =>'form',
                'title'    => 'Edit pengguna',
                'button'   => 'Update',
                'action'   => site_url('pengguna/update_action'),
                'id_inc'   => set_value('id_inc', $row->id_inc),
                'nama'     => set_value('nama', $row->nama),
                'username' => set_value('username', $row->username),
                'role'     =>$this->Model_pengguna->get_role(),
                'vrole'    =>$vrole
	    );
            // $this->load->view('pengguna/view_form', $data);
            $this->template->load('partial/konten_layout','pengguna/view_form',$data);
        } else {
                set_flashdata('warning','Record Not Found');
                redirect(site_url('pengguna'));
        }
    }
    
    public function update_action() 
    {


        $this->cekAkses('update');
        $this->_rules_update();


        if ($this->form_validation->run() == FALSE) {
            $this->update(acak($this->input->post('id_inc', TRUE)));
        } else {

            if($this->cek_role()==0){
                set_flashdata('warning','Data gagal di simpan. karena belum memilih role');
                // redirect(site_url('pengguna'));
            }

            $this->db->trans_start();

            $data = array(
                'nama'     => $this->input->post('nama',TRUE),
                'username' => $this->input->post('username',TRUE),
        	    );

            $this->Model_pengguna->update($this->input->post('id_inc', TRUE), $data);

            

            $this->db->query("DELETE FROM ms_assign_role WHERE ms_pengguna_id=".$this->input->post('id_inc', TRUE));

            for($i=0;$i<count($this->input->post('assign')); $i++){
                $role=array(
                    'ms_pengguna_id'=>$this->input->post('id_inc', TRUE),
                    'ms_role_id'=>$this->input->post('assign')[$i]
                );
                $this->db->insert('ms_assign_role',$role);
            }

               $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE){
                set_flashdata('warning','Data gagal di simpan.');
            
            }else{
                set_flashdata('success','Data berhasil di simpan.');
            }
            redirect(site_url('pengguna'));
        }
    }
    
    public function delete($ide) 
    {
        $this->cekAkses('delete');
        $id  =rapikan($ide);
        $row = $this->Model_pengguna->get_by_id($id);

        if ($row) {
            $res=$this->Model_pengguna->delete($id);
            if($res){
                set_flashdata('success','Data berhasil di hapus.');
            }else{
                set_flashdata('warning','Data gagal di hapus.');
            }
            redirect(site_url('pengguna'));
        } else {
            set_flashdata('warning','Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function _rules() 
    {
    	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
    	$this->form_validation->set_rules('username', 'username', 'trim|required');
    	$this->form_validation->set_rules('password', 'password', 'trim|required');
        
    	$this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
    	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function _rules_update() 
    {
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('username', 'username', 'trim|required');
        
        $this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


    function cek_role(){
        return count($this->input->post('assign'));
    }

    function cek_pass(){
        if($this->input->post('password')==$this->input->post('confirm')){
            return 1;
        }else{
            return 0;
        }
    }



}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */