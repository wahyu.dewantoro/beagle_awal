<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Mauth');
    }
	
	public function index()
	{
		$this->load->view('form-login');
	}

	function proses(){
		if(!empty($this->input->post('username',true)) && $this->input->post('password',true)){
			$username=$this->input->post('username',true);
			$password=sha1($this->input->post('password',true));
			$data = array('password' => $password,'username' =>$username);
			$res=$this->Mauth->proseslogin($data);
			if($res=='true'){
				redirect('welcome');

			}else{
				redirect('auth');	
			}
		}else{
			// echo "asd";
			redirect('auth');
		}
	}

	function logout(){
		session_destroy();
		redirect('auth');
	}
}
