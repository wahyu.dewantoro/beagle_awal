<?php 
$string="<div class=\"be-content\">
            <div class=\"page-head\">
              <h2 class=\"page-head-title\"><?= \$title ?></h2>
              <nav aria-label=\"breadcrumb\" role=\"navigation\">
                <ol class=\"breadcrumb page-head-nav\">
                  <li class=\"breadcrumb-item active\"><?= \$title ?></li>
                </ol>
              </nav>
            </div>
              <div class=\"main-content container-fluid\">
                <div class=\"row\">
                    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">                       
                        <div class=\"card card-border-color card-border-color-primary\">
                            <div class=\"card-body\">
                                <form action=\"<?php echo \$action; ?>\" method=\"post\">";
                foreach ($non_pk as $row) {
                    if ($row["data_type"] == 'text')
                    {
$string .= "\n\t                                   <div class=\"form-group\">
                                        <label for=\"".$row["column_name"]."\">".label($row["column_name"])." <?php echo form_error('".$row["column_name"]."') ?></label>
                                        <textarea class=\"form-control form-control-xs\" rows=\"3\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\"><?php echo $".$row["column_name"]."; ?></textarea>
                                    </div>";
                    } else
                    {
$string .= "\n\t                                    <div class=\"form-group\">
                                            <label for=\"".$row["data_type"]."\">".label($row["column_name"])." <?php echo form_error('".$row["column_name"]."') ?></label>
                                            <input type=\"text\" class=\"form-control form-control-xs\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\" value=\"<?php echo $".$row["column_name"]."; ?>\" />
                                        </div>";
                    }
                }
$string .= "\n\t                                   <input type=\"hidden\" name=\"".$pk."\" value=\"<?php echo $".$pk."; ?>\" /> ";
$string .= "\n\t                                   <button type=\"submit\" class=\"btn btn-sm btn-primary\"><i class=\"mdi mdi-cloud-done\"></i> <?php echo \$button ?></button> ";
$string .= "\n\t                                   <a class=\"btn btn-sm btn-warning\" href=\"<?php echo site_url('".$c_url."') ?>\" ><i class=\"mdi mdi-close\"></i> Cancel</a>";
$string .= "\n\t                            </form>
                            </div>                                                      
                        </div><!-- end card-->                  
                    </div>
                </div>
            </div>
        </div>";
$hasil_view_form = createFile($string, $target."views/" . $c_url . "/" . $v_form_file);

?>