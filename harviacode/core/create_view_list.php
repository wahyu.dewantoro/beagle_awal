<?php 

 
$string="<div class=\"be-content\">
        <div class=\"page-head\">
          <h2 class=\"page-head-title\"><?= \$title ?></h2>
          <nav aria-label=\"breadcrumb\" role=\"navigation\">
            <ol class=\"breadcrumb page-head-nav\">
              <li class=\"breadcrumb-item active\"><?= \$title ?></li>
            </ol>
          </nav>
        </div>
          <div class=\"main-content container-fluid\">
    
            <div class=\"row\">
                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">                       
                    <div class=\"card card-table\">
                        <div class=\"card-header\">
                            Data
                             <div class=\"tools float-right\">
                                <form action=\"<?php echo site_url('$c_url/index'); ?>\" class=\"form-inline\" method=\"get\">
                                    <div class=\"input-group\">
                                        <input type=\"text\" class=\"form-control form-control-xs\" name=\"q\" value=\"<?php echo \$q; ?>\" placeholder=\"Pencarian\">
                                            <span class=\"input-group-btn\">
                                            <div class=\"btn-group\">
                                            
                                                <button class=\"btn btn-primary\" type=\"submit\"><i class=\"mdi mdi-search\"></i> Cari</button>
                                                <?php if (\$q <> '')  { ?>
                                                    <a href=\"<?php echo site_url('$c_url'); ?>\" class=\"btn btn-warning\"><i class=\"mdi mdi-close\"></i> Reset</a>
                                              <?php } 
                                              
                                                    if(\$akses['create']==1){
                                                     echo anchor(site_url('".$c_url."/create'),'<i class=\"mdi mdi-plus\"></i> Tambah', 'class=\"btn btn-success \"');
                                                    }
                                                 ?>
                                            </div>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class=\"card-body\">
                           
                                <table class=\"table table-striped table-bordered\">
                                    <thead>
                                        <tr>
                                            <th width=\"10px\">No</th>";
                                            foreach ($non_pk as $row) {
                                            $string .= "\n\t\t\t\t\t\t\t\t<th>" . label($row['column_name']) . "</th>";
                                            }
                                            $string .= "\n\t\t\t\t\t\t\t\t<th width=\"20px \"> </th>
                                        </tr>
                                    </thead>
                                    <tbody>";
                                        $string .= "\n\t\t\t\t\t\t\t<?php foreach ($" . $c_url . "_data as \$rk)  { ?>
                                        <tr>";
                                        $string .= "\n\t\t\t\t\t\t\t\t<td  align=\"center\"><?php echo ++\$start ?></td>";
                                        foreach ($non_pk as $row) {
                                        $string .= "\n\t\t\t\t\t\t\t\t<td><?php echo \$rk->". $row['column_name'] . " ?></td>";
                                        }
            $string .= "\n\t\t\t\t\t\t\t\t<td style=\"text-align:center\" >"
                    ."\n\t\t\t\t\t\t\t\t\t<div class=\"btn-group\">"
                    . "\n\t\t\t\t\t\t\t\t\t\t<?php "
                    . "\n\t\t\t\t\t\t\t\t\t\t if(\$akses['read']==1){ echo anchor(site_url('".$c_url."/read/'.acak(\$rk->".$pk.")),'<i class=\"mdi mdi-search\"></i>','class=\"btn btn-xs btn-primary\"'); }"
                    . "\n\t\t\t\t\t\t\t\t\t\t if(\$akses['update']==1){ echo anchor(site_url('".$c_url."/update/'.acak(\$rk->".$pk.")),'<i class=\"mdi mdi-edit\"></i>','class=\"btn btn-xs btn-success\"'); }"
                    . "\n\t\t\t\t\t\t\t\t\t\t if(\$akses['delete']==1){ echo anchor(site_url('".$c_url."/delete/'.acak(\$rk->".$pk.")),'<i class=\"mdi mdi-delete\"></i>','class=\"btn btn-xs btn-danger\" onclick=\"javasciprt: return confirm(\\'apakah anda yakin untuk menghapus ?\\')\"'); }"
                    . "\n\t\t\t\t\t\t\t\t\t\t?>"
                    . "\n\t\t\t\t\t\t\t\t\t</div>"
                    ."\n\t\t\t\t\t\t\t\t</td>";
            $string .=  "\n\t\t\t\t\t\t\t</tr>"
                                ."\n\t\t\t\t\t\t\t<?php  }   ?>";
            $string.="\n\t\t\t\t\t\t</tbody>"
                ."\n\t\t\t\t\t</table>"
                ."\n\t\t\t\t\t<button  class=\"btn  btn-space btn-secondary\" disabled>Total Record : <?php echo \$total_rows ?></button>"
                ."\n\t\t\t\t\t<div class=\"float-right\">"
                ."\n\t\t\t\t\t\t<?php echo \$pagination ?>"
                ."\n\t\t\t\t\t</div>
                        </div>                                                      
                    </div><!-- end card-->                  
                </div>
            </div>
    </div>
</div>
";


$hasil_view_list = createFile($string, $target."views/" . $c_url . "/" . $v_list_file);

?>