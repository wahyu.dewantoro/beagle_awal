<?php 
 
$string="<div class=\"be-content\">
        <div class=\"page-head\">
          <h2 class=\"page-head-title\"><?= \$title ?></h2>
          <nav aria-label=\"breadcrumb\" role=\"navigation\">
            <ol class=\"breadcrumb page-head-nav\">
              <li class=\"breadcrumb-item active\"><?= \$title ?></li>
            </ol>
          </nav>
        </div>
          <div class=\"main-content container-fluid\"><div class=\"row\">
    <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">                       
        <div class=\"card card-table\">
            <div class=\"card-body\">
<table class=\"table\">";
foreach ($non_pk as $row) {
    $string .= "\n\t    <tr><td>".label($row["column_name"])."</td><td><?php echo $".$row["column_name"]."; ?></td></tr>";
}
$string .= "\n\t</table>
                </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>
    </div>
</div>";

/*
$string = "<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel=\"stylesheet\" href=\"<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>\"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style=\"margin-top:0px\">".ucfirst($table_name)." Read</h2>
        <table class=\"table\">";
foreach ($non_pk as $row) {
    $string .= "\n\t    <tr><td>".label($row["column_name"])."</td><td><?php echo $".$row["column_name"]."; ?></td></tr>";
}
$string .= "\n\t    <tr><td></td><td><a href=\"<?php echo site_url('".$c_url."') ?>\" class=\"btn btn-default\">Cancel</a></td></tr>";
$string .= "\n\t</table>
        </body>
</html>";

*/

$hasil_view_read = createFile($string, $target."views/" . $c_url . "/" . $v_read_file);

?>